/**
 * @file RF24_UART.cpp
 * @brief The implementation of class RF24_UART. 
 * @author Wu Pengfei<pengfei.wu@itead.cc> 
 * @date 2015.02
 * 
 * @par Copyright:
 * Copyright (c) 2015 ITEAD Intelligent Systems Co., Ltd. \n\n
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version. \n\n
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "RF24_uart.h"

#define LOG_OUTPUT_DEBUG            (1)
#define LOG_OUTPUT_DEBUG_PREFIX     (1)

#define logDebug(arg)\
    do {\
        if (LOG_OUTPUT_DEBUG)\
        {\
            if (LOG_OUTPUT_DEBUG_PREFIX)\
            {\
                Serial.print("[LOG Debug: ");\
                Serial.print((const char*)__FILE__);\
                Serial.print(",");\
                Serial.print((unsigned int)__LINE__);\
                Serial.print(",");\
                Serial.print((const char*)__FUNCTION__);\
                Serial.print("] ");\
            }\
            Serial.print(arg);\
        }\
    } while(0)

#ifdef RF24_USE_SOFTWARE_SERIAL
RF24_UART::RF24_UART(SoftwareSerial &uart, uint32_t baud): m_puart(&uart)
{
    m_puart->begin(baud);
    setBaudRate(baud);
    rx_empty();
}
#else
RF24_UART::RF24_UART(HardwareSerial &uart, uint32_t baud): m_puart(&uart)
{
    m_puart->begin(baud);
    rx_empty();
}
#endif

bool RF24_UART::kick(void)
{
    return eAT();
}

bool RF24_UART::setRxAdd(const uint8_t *addr, uint32_t len)
{
    return sATRXADDR(addr, len);
}

bool RF24_UART::setTxAdd(const uint8_t *addr, uint32_t len)
{
    return sATTXADDR(addr, len);
}

bool RF24_UART::setBaudRate(uint16_t baudrate)
{
    return sATBAUDRATE(baudrate);
}

bool RF24_UART::setComRate(uint16_t rate)
{
    return sATCOMRATE(rate);
}

bool RF24_UART::setFreq(float freq)
{
    return sATFREQ(freq);
}

bool RF24_UART::setCRC(uint16_t crc)
{
    return sATCRC(crc);
}

bool RF24_UART::send(const uint8_t *buffer, uint32_t len)
{
    return sATSEND(buffer, len);
}

uint32_t RF24_UART::recv(uint8_t *buffer, uint32_t buffer_size, uint32_t timeout)
{
    return recvPkg(buffer, buffer_size, timeout);
}

/*----------------------------------------------------------------------------*/
/* +IPD,<id>,<len>:<data> */
/* +IPD,<len>:<data> */

uint32_t RF24_UART::recvPkg(uint8_t *buffer, uint32_t buffer_size, uint32_t timeout)
{
    String data;
    char a;
    uint64_t start;
    
    if (buffer == NULL) {
        return 0;
    }
    
    start = millis();
    uint16_t i = 0;
    while ((millis() - start) < timeout) {
      if(i < buffer_size && m_puart->available() > 0){
        a = m_puart->read();
        buffer[i] = a;
        i++;
      }
    }
    for ( ; i < buffer_size; i++)
    {
        buffer[i] = (char)0;
    }
    return 0;
}

void RF24_UART::rx_empty(void) 
{
    while(m_puart->available() > 0) {
        m_puart->read();
    }
}

String RF24_UART::recvString(String target, uint32_t timeout)
{
    String data;
    char a;
    unsigned long start = millis();
    while (millis() - start < timeout) {
        while(m_puart->available() > 0) {
            a = m_puart->read();
			if(a == '\0') continue;
            data += a;
        }
        if (data.indexOf(target) != -1) {
            break;
        }   
    }
    return data;
}

String RF24_UART::recvString(String target1, String target2, uint32_t timeout)
{
    String data;
    char a;
    unsigned long start = millis();
    while (millis() - start < timeout) {
        while(m_puart->available() > 0) {
            a = m_puart->read();
			if(a == '\0') continue;
            data += a;
        }
        if (data.indexOf(target1) != -1) {
            break;
        } else if (data.indexOf(target2) != -1) {
            break;
        }
    }
    return data;
}

String RF24_UART::recvString(String target1, String target2, String target3, uint32_t timeout)
{
    String data;
    char a;
    unsigned long start = millis();
    while (millis() - start < timeout) {
        while(m_puart->available() > 0) {
            a = m_puart->read();
			if(a == '\0') continue;
            data += a;
        }
        if (data.indexOf(target1) != -1) {
            break;
        } else if (data.indexOf(target2) != -1) {
            break;
        } else if (data.indexOf(target3) != -1) {
            break;
        }
    }
    return data;
}

bool RF24_UART::recvFind(String target, uint32_t timeout)
{
    String data_tmp;
    data_tmp = recvString(target, timeout);
    if (data_tmp.indexOf(target) != -1) {
        return true;
    }
    return false;
}

bool RF24_UART::recvFindAndFilter(String target, String begin, String end, String &data, uint32_t timeout)
{
    String data_tmp;
    data_tmp = recvString(target, timeout);
    if (data_tmp.indexOf(target) != -1) {
        int32_t index1 = data_tmp.indexOf(begin);
        int32_t index2 = data_tmp.indexOf(end);
        if (index1 != -1 && index2 != -1) {
            index1 += begin.length();
            data = data_tmp.substring(index1, index2);
            return true;
        }
    }
    data = "";
    return false;
}

bool RF24_UART::eAT(void)
{
    rx_empty();
    m_puart->println("AT?");
    return recvFind("OK");
}

bool RF24_UART::sATSEND(const uint8_t *buffer, uint16_t len)
{
    rx_empty();
    // m_puart->write(buffer);
    m_puart->write(buffer,len);
    Serial.println(len);
    for (uint16_t i = 0; i < len; i++) {
        // m_puart->write(buffer[i]);
        //m_puart->write((char)buffer[i]);
        //delay(2);
    }
    return true;
}

bool RF24_UART::sATCRC(uint16_t crc) {
    m_puart->print("AT+CRC=");
    m_puart->println(crc);
    return true;
}

bool RF24_UART::sATCOMRATE(uint16_t rate) {
    uint16_t rate_sec = 0;
    if(rate == 250)
        rate_sec = 1;
    else if (rate == 1)
        rate_sec = 2;
    else if (rate == 2)
        rate_sec = 3;
    else
        return false;
    
    m_puart->print("AT+RATE=");
    m_puart->println(rate_sec);
    return true;
}

bool RF24_UART::sATBAUDRATE(uint16_t baudrate) {
    uint16_t rate_sec = 0;
    if(baudrate == 4800)
        rate_sec = 1;
    else if (baudrate == 9600)
        rate_sec = 2;
    else if (baudrate == 14400)
        rate_sec = 3;
    else if (baudrate == 19200)
        rate_sec = 4;
    else if (baudrate == 38400)
        rate_sec = 5;
    else if (baudrate == 57600)
        rate_sec = 6;
    else if (baudrate == 115200)
        rate_sec = 7;
    else
        return false;
    
    m_puart->print("AT+BUAD=");
    m_puart->println(rate_sec);
    return true;
}

bool RF24_UART::sATFREQ(float freq) {
    m_puart->print("AT+FREQ=");
    m_puart->print(freq,3);
    m_puart->println("G");
    return true;
}

bool RF24_UART::sATRXADDR(const uint8_t *addr, uint16_t len) {
    if(len>=10) {
        m_puart->write("AT+RXA=");
        m_puart->write("0x");
        m_puart->write(addr[0]);
        m_puart->write(addr[1]);
        m_puart->write(",0x");
        m_puart->write(addr[2]);
        m_puart->write(addr[3]);
        m_puart->write(",0x");
        m_puart->write(addr[4]);
        m_puart->write(addr[5]);
        m_puart->write(",0x");
        m_puart->write(addr[6]);
        m_puart->write(addr[7]);
        m_puart->write(",0x");
        m_puart->write(addr[8]);
        m_puart->write(addr[9]);
    }
    else
        return false;
    
    return true;
}

bool RF24_UART::sATTXADDR(const uint8_t *addr, uint16_t len) {
    if(len>=10) {
        m_puart->write("AT+TXA=");
        m_puart->write("0x");
        m_puart->write(addr[0]);
        m_puart->write(addr[1]);
        m_puart->write(",0x");
        m_puart->write(addr[2]);
        m_puart->write(addr[3]);
        m_puart->write(",0x");
        m_puart->write(addr[4]);
        m_puart->write(addr[5]);
        m_puart->write(",0x");
        m_puart->write(addr[6]);
        m_puart->write(addr[7]);
        m_puart->write(",0x");
        m_puart->write(addr[8]);
        m_puart->write(addr[9]);
    }
    else
        return false;
    
    return true;
}


