@mainpage

# NRF24L01+ Using UART to communicate

An Nrf library for Arduino providing an easy-to-use way to manipulate nrf24l01 by using uart.

# Source 

Source can be download at <https://gitlab.com/Ernie_Wang/nrf24_with_uart>.

You can clone it by:

    git clone https://gitlab.com/Ernie_Wang/nrf24_with_uart.git

# Documentation

TTL to NRF24L01+ board can be bought from here <https://shopee.tw/%EF%BC%88%E7%8F%BE%E8%B2%A8%E9%99%84%E7%99%BC%E7%A5%A8%EF%BC%89nRF24L01-%E6%A8%A1%E7%B5%84%E8%BD%89%E6%8E%A5%E6%9D%BF-nRF24L01-%E8%BD%89-TTL-%E9%8A%9C%E6%8E%A5%E9%96%8B%E7%99%BC%E6%9D%BF-i.10207300.864158506?fbclid=IwAR1HN5rRfiwYj9AS6rsIL5VkKxKrpMyxmGceMlT6uooyz9xNP2r16Rernxc>

Online API documentation can be reached at <https://onedrive.live.com/?authkey=%21ADi19igyKgyKwus&cid=C728A6902F88FF97&id=C728A6902F88FF97%21388&parId=C728A6902F88FF97%21107&action=locate>.

# How to get started

On the home page of API documentation, the tabs of Examples, Classes and Modules 
will be useful for Arduino lovers. 

# API List

    bool 	    kick (void) : Verify nrf whether live or not.
    
    bool        setRxAdd(const uint8_t *addr, uint32_t len) : Set recieve address
    
    bool        setTxAdd(const uint8_t *addr, uint32_t len) : Set transmit address
    
    bool        setBaudRate(uint16_t baudrate) : Set baudrate
  
    bool        setComRate(uint16_t rate) : Set transmission rate
  
    bool        setFreq(float freq) : Set transmit frequency
    
    bool        setCRC(uint16_t crc) : Set CRC
    
    bool        send(const uint8_t *buffer, uint32_t len) : Send data through RF24

    uint32_t    recv(uint8_t *buffer, uint32_t buffer_size, uint32_t timeout = 1000) : Get recieve data

# Mainboard Requires

  - RAM: not less than 2KBytes
  - Serial: one serial (HardwareSerial or SoftwareSerial) at least 

# Suppported Mainboards
  
  - Arduino Micro (other haven't been test yet)

# Using SoftwareSerial

If you want to use SoftwareSerial to communicate with ESP8266, you need to modify
the line in file `RF24_uart.h`: 

    //#define RF24_USE_SOFTWARE_SERIAL

After modification, it should be:

    #define RF24_USE_SOFTWARE_SERIAL


# Hardware Connection

All communications are done via uart. You must specify the uart used by mainboard 
to communicate with RF24.


## UNO

To use SoftwareSerial, `mySerial` will be used if you create an object (named RF24)
of class RF24_UART in your code like this:

    #include "RF24_uart.h"
    #include <SoftwareSerial.h>
    
    SoftwareSerial mySerial(3, 2); /* RX:D3, TX:D2 */
    RF24_UART RF24(mySerial);

The connection should be like these:

    RF24_UART_TX->RX(D3)
    RF24_UART_RX->TX(D2)
    RF24_UART_VCC->3.3V
    RF24_UART_GND->GND
    
# Attention
~~The voltage of rf to ttl board must be connect to 3.3V, though it may work in 5V, but it might potentially break the arduino~~

# Contributer

[CYYang](https://github.com/NekoSaiKou)  

-------------------------------------------------------------------------------

# The End!

-------------------------------------------------------------------------------
