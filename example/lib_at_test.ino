#include  <SoftwareSerial.h>
#include "RF24_uart.h"
SoftwareSerial RFSerial(10, 11); // RX 、 TX
RF24_UART RF24(RFSerial, 9600);
char addr[2][10] = {"BBCCDDEEFF", "06FFFFFFFF"};
char set[] = {'A', 'T', '?'};
void setup()
{
  Serial.begin(9600);
  
  RFSerial.begin(9600);
  delay(1000);
  Serial.println(RF24.kick());
  delay(1000);
  
  RF24.setTxAdd(addr[0], sizeof(addr[0]));
  //  RFSerial.write("AT+TXA=0xaa,0xff,0xff,0xff,0xff");
  delay(1500);
  while (RFSerial.available()) {
    Serial.print((char)RFSerial.read());
  }
  delay(1000);

  RF24.setRxAdd(addr[1], sizeof(addr[1]));
  //  RFSerial.write("AT+RXA=0xaa,0xff,0xff,0xff,0xff");
  while (RFSerial.available()) {
    Serial.print((char)RFSerial.read());
  }
  delay(1500);

  RF24.setFreq(2.525);
  //RFSerial.print("AT+FREQ=2.400G");
  while (RFSerial.available()) {
    Serial.print((char)RFSerial.read());
  }
  delay(1000);
  
  RF24.setComRate(250);
  //RFSerial.print("AT+RATE=1");
  while (RFSerial.available()) {
    Serial.print((char)RFSerial.read());
  }
  delay(1000);

  RF24.setCRC(16);
  //RFSerial.print("AT+CRC=16");
  while (RFSerial.available()) {
    Serial.print((char)RFSerial.read());
  }
  delay(1000);

  RF24.setBaudRate(9600);
  //RFSerial.print("AT+BAUD=2");
  while (RFSerial.available()) {
    Serial.print((char)RFSerial.read());
  }
  
  Serial.println(RF24.kick());
  Serial.println("finished");
}
char Reserve[32] = {};
char Buff[32] = {};
void loop()
{
  if (RFSerial.available()) {
    Serial.print("RF available:  ");
    RF24.recv(Buff, sizeof(Buff), 30);
    Serial.println(Buff);
  }
  if (Serial.available()) {
    Serial.println("send mesg");
    //  int j = 1;
    int j = 0;
    while (Serial.available()) {
      Buff[j] = Serial.read();
      j++;
    }
    RF24.send(Buff, j);

    for (; j < sizeof(Buff); j++) {
      Buff[j] = (char)0;
    }
    Serial.println(Buff);
  }
}
